package com.example.lynn.gradient;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;

import static com.example.lynn.gradient.MainActivity.*;

/**
 * Created by lynn on 4/1/2017.
 */

public class MyListener implements View.OnClickListener {

    public int getColor() {
        int red = (int)(256*Math.random());
        int green = (int)(256*Math.random());
        int blue = (int)(256*Math.random());

        return(Color.argb(255,red,green,blue));
    }

    @Override
    public void onClick(View view) {
        int[] colors = {getColor(),getColor()};

        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,colors);

        myView.setBackground(drawable);
    }

}
